import * as express from 'express';

export class Controller {
    home(request: express.Request, response: express.Response) {
        response.send('Hello World!');
    }

    list(request: express.Request, response: express.Response) {
        response.json([
            {
                name: 'Async Drink'
            },
            {
                name: 'Mathyn Buiteveld'
            },
            {
                name: 'Someone else'
            }
        ]);
    }
}

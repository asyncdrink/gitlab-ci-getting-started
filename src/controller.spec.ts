import { Controller } from "./controller";

describe('controller', () => {
    let controller: Controller;

    beforeEach(() => {
        controller = new Controller();
    })

    it('should send \'Hello World!\' from the home page', () => {
        const mockRequest = {} as any;
        const mockResponse = {send: () => {}} as any;

        spyOn(mockResponse, 'send');

        controller.home(mockRequest, mockResponse);

        expect(mockResponse.send).toHaveBeenCalledWith('Hello World!');
    });

    it('should send a list from the list page', () => {
        const mockRequest = {} as any;
        const mockResponse = {json: () => {}} as any;

        spyOn(mockResponse, 'json');

        controller.list(mockRequest, mockResponse);

        expect(mockResponse.json).toHaveBeenCalledWith([
            {
                name: 'Async Drink'
            },
            {
                name: 'Mathyn Buiteveld'
            },
            {
                name: 'Someone else'
            }
        ]);
    });
})
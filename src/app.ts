import * as express from 'express';
import { Controller } from './controller';

const app = express();
const port = 3000;

const controller = new Controller();

app.get('/', (request, response) => controller.home(request, response));
app.get('/list', (request, response) => controller.list(request, response));

app.listen(port, () => console.log(`App started on port ${ port }!`));
